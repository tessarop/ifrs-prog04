package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/* TODO 2 - Implemente 'EventHandler<ActionEvent>' */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Button btn = new Button("Click");
        /* TODO 1 - Chame 'btn.setOnAction(this)' */

        StackPane container = new StackPane();
        container.getChildren().add(btn);

        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(container, 640, 480));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    /* TODO 3 - Sobreescreva 'handle' */
    /* TODO 5 - Dentro de 'handle' */
    /* TODO 4 - Implemente uma ação. Ex.: System.out.println */
    /* TODO 6 - Rode o código (Shift + F10) */
    /* TODO 5 - Faça o seguinte código funcionar:
        if (actionEvent.getSource() == btn) {
            System.out.println("Hello World");
        }
    */
}
