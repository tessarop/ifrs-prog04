package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import sample.model.Person;

import java.io.IOException;

public class PersonListCellController extends ListCell<Person> {
    private Node graphic;

    // TODO (9) - Declare e ligue duas Label's ao FXML 'PersonListCellView'

    PersonListCellController() {
        // TODO (7) - Carregue a view 'PersonListCellView' na variável chamada 'loader'
        // TODO (8) - Configure o controller com 'loader.setController'

        try {
            graphic = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void updateItem(Person item, boolean empty) {
        super.updateItem(item, empty);

        if (item == null || empty || graphic == null) {
            setText(null);
            setGraphic(null);
        } else {
            // TODO (10) - Exiba o primeiro e último nomes

            setGraphic(graphic);
        }
    }
}
